package processors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.CommentRemover;
import exception.CommentRemoverException;
import handling.RegexSelector;

public class JavaFileProcessor extends AbstractFileProcessor {

    private static final String regex;
    private static final String singleLineCommentSymbol;
    private static final String singleLineCommentEscapeToken;
    private static final String singleLineCommentEscapePrefix;

    static {
        regex = RegexSelector.getRegexByFileType("java");
        singleLineCommentSymbol = "//";
        singleLineCommentEscapeToken = "//" + UUID.randomUUID().toString();
        singleLineCommentEscapePrefix = UUID.randomUUID().toString();
    }

    public JavaFileProcessor(CommentRemover commentRemover) {
        super(commentRemover);
    }

    @Override
    public void replaceCommentsWithBlanks() throws IOException, CommentRemoverException {
        super.replaceCommentsWithBlanks(regex);
    }

    @Override
    protected StringBuilder getFileContent(File file) throws IOException, CommentRemoverException {
        return isGoingToRemoveSingleComments() ? this.getContentForSingleLinesRemoving(file)
                : super.getPlainFileContent(file);
    }

    private boolean isGoingToRemoveSingleComments() {
        return commentRemover.isRemoveSingleLines();
    }

    private StringBuilder getContentForSingleLinesRemoving(File file) throws IOException {

        StringBuilder content = new StringBuilder((int) file.length());

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        for (String temp = br.readLine(); temp != null; temp = br.readLine()) {

            String trimmedTemp = temp.trim();
            if (trimmedTemp.startsWith(singleLineCommentSymbol) && !isContainTaskTag(trimmedTemp)) {
                content.append(singleLineCommentEscapeToken).append("\n");
            } else {
                content.append(temp).append("\n");
            }
        }
        br.close();

        return content;
    }

    @Override
    protected StringBuilder doRemoveOperation(StringBuilder fileContent, Matcher matcher) throws StackOverflowError {

        String sFileContent = fileContent.toString();
        boolean isTaskTagRemoving = commentRemover.isRemoveTaskTags();
        boolean isBothCommentTypeNotSelected = isBothCommentTypeNotSelected();
        boolean isPreserveJavaClassHeader = commentRemover.isPreserveJavaClassHeaders();
        boolean isPreserveCopyrightHeaders = commentRemover.isPreserveCopyrightHeaders();
        while (matcher.find()) {

            String foundToken = matcher.group();

            if (isDoubleOrSingleQuoteToken(foundToken)) {
                continue;
            }

            if (isPreserveJavaClassHeader) {
                if (isClassHeader(foundToken)) {
                    continue;
                }
            }
            if (isPreserveCopyrightHeaders) {
                if (isCopyRightHeader(foundToken)) {
                    continue;
                }
            }

            if (isBothCommentTypeNotSelected) {

                if (isOnlyMultiLineCommentSelected() && isSingleCommentToken(foundToken)) {
                    continue;
                }

                if (isOnlySingleCommentSelected() && isMultiLineCommentToken(foundToken)) {
                    continue;
                }
            }

            if (isTaskTagRemoving) {
                if (isSingleLineTaskTagToken(foundToken)) {
                    sFileContent = replaceSingleLineTodoComment(sFileContent, foundToken);
                } else {
                    sFileContent = replaceSingleLineComment(sFileContent, foundToken);
                }
            } else {

                if (commentRemover.isRemoveSingleLines() && isSingleCommentToken(foundToken)) {
                    sFileContent = replaceSingleLineComment(sFileContent, foundToken);
                } else if (commentRemover.isRemoveMultiLines() && isMultiLineCommentToken(foundToken)) {
                    sFileContent = replaceMultiLineComment(sFileContent, foundToken);
                }

            }
        }

        // if (!isTaskTagRemoving) {
        // sFileContent = sFileContent.replace(singleLineCommentEscapePrefix,
        // "//");
        // }

        fileContent = new StringBuilder(sFileContent);

        return fileContent;
    }

    private String replaceMultiLineComment(String sFileContent, String foundToken) {
        return sFileContent.replaceFirst(Pattern.quote(foundToken), "");
    }

    private String replaceSingleLineComment(String sFileContent, String foundToken) {
        return sFileContent.replaceFirst(Pattern.quote(foundToken), "");
    }

    private String replaceSingleLineTodoComment(String sFileContent, String foundToken) {
        return sFileContent.replaceFirst(Pattern.quote(foundToken),
                foundToken.replace("//", singleLineCommentEscapePrefix));
    }

    private boolean isBothCommentTypeNotSelected() {
        return !(commentRemover.isRemoveSingleLines() && commentRemover.isRemoveMultiLines());
    }

    private boolean isDoubleOrSingleQuoteToken(String foundToken) {
        return foundToken.startsWith("\"") || foundToken.startsWith("\'");
    }

    private boolean isOnlyMultiLineCommentSelected() {
        return !commentRemover.isRemoveSingleLines() && commentRemover.isRemoveMultiLines();
    }

    private boolean isSingleCommentToken(String foundToken) {
        return foundToken.startsWith("//");
    }

    private boolean isOnlySingleCommentSelected() {
        return commentRemover.isRemoveSingleLines() && !commentRemover.isRemoveMultiLines();
    }

    private boolean isMultiLineCommentToken(String foundToken) {
        return foundToken.startsWith("/*");
    }

    private boolean isSingleLineTaskTagToken(String foundToken) {
        return isSingleCommentToken(foundToken) && isContainTaskTag(foundToken);
    }

    private boolean isCopyRightHeader(String foundToken) {
        return Pattern.compile(Pattern.quote("copyright"), Pattern.CASE_INSENSITIVE).matcher(foundToken).find();
    }

    private boolean isClassHeader(String foundToken) {
        return Pattern.compile(Pattern.quote("author"), Pattern.CASE_INSENSITIVE).matcher(foundToken).find();
    }
}